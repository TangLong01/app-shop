import { useEffect } from "react";
import "tailwindcss/tailwind.css";
import { getAllProducts, getProductById } from "../services/products";

export default function Home() {
  useEffect(() => {
    getAllProducts().then((res) => {
      console.log(res.data);
    });
    getProductById(1).then((res) => {
      console.log(res.data);
    });
  }, []);
  return <div className="text-3xl font-bold underline">Hello world!</div>;
}
