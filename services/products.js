import request from "../utils/api";

export const getAllProducts = () => {
  return request("/products");
};

export const getProductById = (id) => {
  return request("/products", id);
};
