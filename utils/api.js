import axios from "axios";

const baseUrl = "https://fakestoreapi.com";

const request = (url, params) =>
  axios({ url: `${baseUrl}${url}${params ? `/${params}` : ""}` });

export default request;
